<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'template');

/** MySQL database username */
define('DB_USER', 'template');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Bva]/a{?%4ejuKd-y$of/;0>_>q)<p?ElgP2#!3EP${6MnIVY~KDTr%lo88imhAU');
define('SECURE_AUTH_KEY',  'u}mJ8As~!YZgck,g/#kMeASMSlfK#$VXbV,&l:e,rKb+BzxP:TRy}sN0X;aH:nxi');
define('LOGGED_IN_KEY',    '#O|FgH& 9!xI^y+X?n6#xiUU&|Cj:/~$YZ!*iXRa**7K~--/yb?t*t=nXS{BYXVi');
define('NONCE_KEY',        'Q 5r0NR8rY[f(elYfQc_cA-MP5tk&tM%]q@2{9-%phkUt@3~K9cR nJNtRQ|vD$o');
define('AUTH_SALT',        'J~6T:>l0KbOx>KpmF{ICzz!J:~[8eUep$.3@RB(MRWnmgB6 Um#*dB?O?yR(FbM[');
define('SECURE_AUTH_SALT', 'g]4tYcfi.zOo_!$ZstXPT4OUA7Wv@FBu_AgQr2cc@Oy[b&4JgI1WlU0?pTISXkP^');
define('LOGGED_IN_SALT',   ';Pp85t5,X/o!`EGU5D2bZ|=RM.srH=I3bJ9@t5,ZZP;Iw3QR%B2M=_>55LB,fApN');
define('NONCE_SALT',       'm!d@PzQ*$c]+}U46SA#830;[9i.f$JJl+Fs,]JOuo2x5-g:`0P`;uKz-DqD! c) ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
