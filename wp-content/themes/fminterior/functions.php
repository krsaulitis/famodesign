<?php

function load_materialize()
{
  wp_enqueue_style( 'maincss', get_template_directory_uri() . '/style.css');
  wp_enqueue_script("jquery");
  wp_enqueue_script( 'materializejs', get_template_directory_uri() . '/js/materialize.js', array ( 'jquery' ));
  wp_enqueue_script( 'materializestepperjs', get_template_directory_uri() . '/js/materialize-stepper.js', array ( 'jquery', 'materializejs' ));
}
add_action( 'wp_enqueue_scripts', 'load_materialize' );

function fmdesign_navbar()
{
	register_nav_menus([
		'main' => __('Main menu'),
	]);
}
add_action('after_setup_theme', 'fmdesign_navbar');

add_image_size('quiz', 400, 400, true );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
