<!DOCTYPE html>
<html>
<head>
  <?php wp_head(); ?>

  <!-- Materializecss compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/css/materialize.min.css">
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import Materialize-Stepper CSS -->
  <link rel="stylesheet" href="<?=get_template_directory_uri() . '/css/materialize-stepper.min.css' ?>">
  <!--Style.css-->
  <link rel="stylesheet" href="<?=get_template_directory_uri() . '/stylesheets/style.css' ?>">

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <!-- Materializecss compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.0/js/materialize.min.js"></script>
  <!-- jQueryValidation Plugin (optional) -->
  <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
  <!--Import Materialize-Stepper JavaScript -->
  <script src="<?= get_template_directory_uri() . '/js/materialize-stepper.min.js' ?>"></script>

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons black-text">menu</i></a>
        <div id="nav-pre"></div>
        <div id="nav-main">
          <a href="<?= get_home_url() ?>" class="brand-logo ">FAMODESIGN</a>
            <?php
              wp_nav_menu(['menu_id' => 'main nav-mobile','container'=>false, 'menu_class'=>'right hide-on-med-and-down']);
            ?>
        </div>
        <div id="nav-lang">
          <?php pll_the_languages(['show_names'=> 0, 'show_flags'=>1, 'hide_if_empty'=> 0]); ?>
        </div>
        <?php
          wp_nav_menu(['menu_id' => 'mobile-demo','container'=>false, 'menu_class'=>'side-nav']);
        ?>
      </div>
    </nav>
  </div>

  <script type="text/javascript">
    $(".button-collapse").sideNav({
      draggable: true
    });
  </script>

<body>
