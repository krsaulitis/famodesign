<?php
  get_header();
?>

<div id="main" >
  <div class="option-wrapper">
    <div class="card first">
      <a href="<?= get_home_url() . '/quiz'?>"><h3>Quiz</h3></a>
    </div>
    <div class="card second">
      <a href="<?= get_home_url() . '/quiz'?>"><h3>Order in person</h3></a>
    </div>
  </div>
</div>

<script type="text/javascript" src=<?= get_template_directory_uri() . '/js/index.js'; ?>></script>

<?php
  get_footer();
?>
