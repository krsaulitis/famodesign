<?php
  get_header();
?>

<div id="quiz">

  <h3>Quiz</h3>


  <ul class="stepper horizontal">

    <!-- ROOM STYLE -->
    <?php
      $room_styles = get_field('room_style');
      $room_styles_info = get_field_object('room_style');
    ?>
    <li class="step images active">

      <div class="step-title">
        <?= $room_styles_info['label'] ?>
      </div>

      <div class="step-content">
        <div class="row">
          <?php
          foreach ($room_styles as $room_style) {
          ?>
          <div class="card-box">
            <input type="checkbox" name="style" id="<?= $room_style['title']; ?>">

            <label for="<?= $room_style['title']; ?>">
              <a class="tooltipped" data-position="bottom" data-delay="20" data-tooltip="<?= $room_style['title']; ?>">
                <div class="card">
                    <img src="<?= $room_style['image']['sizes']['quiz'] ?>">
                </div>
              </a>
            </label>

          </div>
          <?php
          }
          ?>
        </div>

        <div class="step-actions">
          <button class=" btn next-step">CONTINUE</button>
        </div>

      </div>
    </li>

    <!-- ROOM TYPE -->
    <?php
      $room_types = get_field('room_types');
      $room_types_info = get_field_object('room_types');
    ?>
    <li class="step icons">

      <div class="step-title">
        <?= $room_types_info['label']; ?>
      </div>

      <div class="step-content">
        <div class="row">
          <?php
          foreach ($room_types as $room_type) {
          ?>
          <div class="card-box">
            <input type="radio" name="style" id="<?= $room_type['title']; ?>">

            <label for="<?= $room_type['title']; ?>">
              <div class="card">
                <h3><?= $room_type['title']; ?></h3>
                <p><?= $room_type['decription'] ?></p>
                <img class="" src="<?= $room_type['icon']['url']?>">
              </div>
            </label>

          </div>
          <?php
          }
          ?>
        </div>
        <div class="step-actions">
          <button class="btn next-step">CONTINUE</button>
          <button class="btn-flat previous-step">BACK</button>
        </div>
      </div>
    </li>

    <!-- ROOM FUNCTION -->
    <?php
      $room_functions = get_field('room_function');
      $room_functions_info = get_field_object('room_function');
    ?>
    <li class="step icons">

      <div class="step-title">
        <?= $room_functions_info['label']; ?>
      </div>

      <div class="step-content">
        <div class="row">
          <?php
          foreach ($room_functions as $room_function) {
          ?>
          <div class="card-box">
            <input type="checkbox" name="style" id="<?= $room_function['title']; ?>">

            <label for="<?= $room_function['title']?>">
              <div class="card">
                <h3><?= $room_function['title']; ?></h3>
                <p><?= $room_function['decription'] ?></p>
                <img class="" src="<?= $room_function['icon']['url']?>">
              </div>
            </label>

          </div>
          <?php
          }
          ?>
        </div>
        <div class="step-actions">
          <button class="btn next-step">CONTINUE</button>
          <button class="btn-flat previous-step">BACK</button>
        </div>
      </div>
    </li>


    <!-- COLORS -->
    <?php
      $room_colors = get_field('colors');
      $room_colors_info = get_field_object('colors');
    ?>
    <li class="step images">

      <div class="step-title">
        <?= $room_colors_info['label']; ?>
      </div>

      <div class="step-content">
        <div class="row">
          <?php
          foreach ($room_colors as $room_color) {
          ?>
          <div class="card-box">
            <input type="checkbox" name="style" id="<?= $room_color['title']; ?>">

            <label for="<?= $room_color['title']; ?>">
              <div class="card">
                <img class="" src="<?= $room_color['image']['sizes']['medium']?>">
              </div>
            </label>
          </div>
          <?php
          }
          ?>
        </div>
        <div class="step-actions">
          <button class="btn next-step">CONTINUE</button>
          <button class="btn-flat previous-step">BACK</button>
        </div>
      </div>
    </li>


    <li class="step">
      <div class="step-title">Plan</div>
      <div class="step-content">
        <div class="step-actions">
            <button class="waves-effect waves-dark btn blue next-step">CONTINUE</button>
            <button class="waves-effect waves-dark btn-flat previous-step">BACK</button>
        </div>
      </div>
    </li>

    <li class="step">
      <div class="step-title">Contacts</div>
      <div class="step-content">
        <div class="step-actions">
            <button class="waves-effect waves-dark btn blue next-step">CONTINUE</button>
            <button class="waves-effect waves-dark btn-flat previous-step">BACK</button>
        </div>
      </div>
    </li>

  </ul>
</div>

<script type="text/javascript" src=<?= get_template_directory_uri() . '/js/quiz.js'; ?>></script>

<?php
  get_footer();
?>
