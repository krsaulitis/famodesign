$(function(){
   $('.stepper').activateStepper();
});

function next(){
	console.log('next');
}

function prev(){
	console.log('prev');
}

$('input:checkbox').change(function(){
    if($(this).is(":checked")) {
        $(this).parent().addClass("checked");
    } else {
        $(this).parent().removeClass("checked");
    }
});

var radios = $('input:radio');

$('input:radio').change(function(){
    for (var i = 0; i < radios.length; i++) {
      if(radios[i].checked)
      {
        $(radios[i]).parent().addClass("checked");
      }
      else
      {
        $(radios[i]).parent().removeClass("checked");
      }
    }
});
